#ifndef __HELLOWORLD_SCENE_H__
#define __HELLOWORLD_SCENE_H__

#include "cocos2d.h"
//射击速度间隔
static const int HERO_BULLET_SPEED = 1000;
//hero射击间隔
static const float HERO_FIRE_DURATION = 0.3f;
//bigEnemy射击速度
static const int ENEMY_BULLET_SPEED = 800;
//BigEnmey射击间隔
static const float ENEMY_FIRE_DURATION = 0.5f;
//创建smaEnmey对象间隔
static const float SMALL_ENEMY_CREATE_DURATION = 1.0f;

//创建bigEnemy对象间隔
static const float BIG_ENEMY_CREATE_DURATION = 3.0f;

//body类型定义
enum  ContactTag
{
	HERO =0 ,
   HERO_BULLET,
  ENEMY,
  ENEMY_BULLET
};




class HelloWorld : public cocos2d::Layer
{
public:
    // 创建场景
    static cocos2d::Scene* createScene();

    // 层的初始化
    virtual bool init();
	//进入层
	void onEnter()override;
	//层清理
	void cleanup()override;

    // 创建层的方法
    CREATE_FUNC(HelloWorld);
	//创建hero
	void createHero();

	//创建smallEnemy
	void createSmallEnemy(float dt);

	//创建BigEnmey
	void createBigEnemy(float dt);
	//射击
	void heroFire(float dt);
	void enemyFire(float dt);
	
	//初始化背景
	void initBackground();


	//移动背景
	void moveBackground(float dt);


private:
	//可视区域大小
	cocos2d::Size visibleSize;
};

#endif // __HELLOWORLD_SCENE_H__
