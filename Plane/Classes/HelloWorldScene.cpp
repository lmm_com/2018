#include "HelloWorldScene.h"

#include "SimpleAudioEngine.h"

USING_NS_CC;
using namespace CocosDenshion;


Scene* HelloWorld::createScene()
{
    // 'scene' is an autorelease object
   // auto scene = Scene::create();
	//创建物理世界场景
	auto scene = Scene::createWithPhysics();
	//创建物理世界重力（取消）
	scene->getPhysicsWorld()->setGravity(Vec2(0, 0));
	//物理世界调试
	scene->getPhysicsWorld()->setDebugDrawMask(PhysicsWorld::DEBUGDRAW_ALL);

    
    // 'layer' is an autorelease object
    auto layer = HelloWorld::create();

    // add layer as a child to scene
    scene->addChild(layer);

    // return the scene
    return scene;
}

// on "init" you need to initialize your instance
bool HelloWorld::init()
{
    //由Layer的create方法调用
    if ( !Layer::init() )
    {
        return false;
    }
	visibleSize = Director::getInstance()->getVisibleSize();

	createHero();
	//createSmallEnemy();

	//createBigEnemy();
	//创建多个Enemy对象
	this->schedule(schedule_selector(HelloWorld::createSmallEnemy), SMALL_ENEMY_CREATE_DURATION);
	this->schedule(schedule_selector(HelloWorld::createBigEnemy), BIG_ENEMY_CREATE_DURATION);
	initBackground();


    return true;
}
void HelloWorld::onEnter() {
	Layer::onEnter();
	SimpleAudioEngine::getInstance()->preloadBackgroundMusic("Audio/background.mp3");
	SimpleAudioEngine::getInstance()->preloadEffect("Audio/shoot.wav");
	SimpleAudioEngine::getInstance()->playBackgroundMusic("Audio/background.mp3", true);
}
void HelloWorld::cleanup() {
	Layer::cleanup();
	SimpleAudioEngine::getInstance()->stopBackgroundMusic();
	SimpleAudioEngine::getInstance()->unloadEffect("Audio/shoot.wav");

}
void  HelloWorld::createHero() {

#pragma region   Hero游戏元素及动画
	auto hero = Sprite::create("Texture/Hero1.png");
	this->addChild(hero);
	hero->setPosition(Vec2(visibleSize.width / 2, hero->getContentSize().height / 2));
	hero->setName("Hero");
	Vector<SpriteFrame*>heroFrames;
	heroFrames.pushBack(SpriteFrame::create("Texture/Hero1.png", Rect(0, 0, 136, 168)));
	heroFrames.pushBack(SpriteFrame::create("Texture/Hero2.png", Rect(0, 0, 136, 168)));
	hero->runAction(RepeatForever::create(Animate::create(Animation::createWithSpriteFrames(heroFrames, 0.3f))));
#pragma endregion
#pragma region Hero触控事件
	//单点触控事件
	auto listener = EventListenerTouchOneByOne::create();
	//处理相应的函数实现 "="表示复制一份外部变量给匿名函数使用
	listener->onTouchBegan = [=](Touch*pTouch, Event*pEvent) {return true; };
	listener->onTouchMoved = [=](Touch*pTouch, Event*pEvent)
	{//获取移动后的新坐标
		auto newsPos = hero->getPosition() + pTouch->getDelta();
		//边界控制
		auto heroSize = hero->getContentSize();
		if (newsPos.x > visibleSize.width - heroSize.width / 2) { newsPos.x = visibleSize.width - heroSize.width / 2; }
		else if (newsPos.x < heroSize.width / 2) {
			newsPos.x = heroSize.width / 2;
		}
		if (newsPos.y > visibleSize.height - heroSize.height / 2) { newsPos.y = visibleSize.height - heroSize.height / 2; }
		else if (newsPos.y < heroSize.height / 2) {
			newsPos.y = heroSize.height / 2;
		}
		//设置新位置
		hero->setPosition(newsPos); };
	//注册监听
	_eventDispatcher->addEventListenerWithSceneGraphPriority(listener, this);
#pragma endregion
#pragma region Hero刚体
	auto size = hero->getContentSize();
	auto body = PhysicsBody::createBox(size);
	//设置标志
	body->setTag(ContactTag::HERO);
	//设置碰撞掩码
	body->setContactTestBitmask(0xFFFFFFFF);
	hero->setPhysicsBody(body);
#pragma endregion

	//调度器：控制射击
	this->schedule(schedule_selector(HelloWorld::heroFire),HERO_FIRE_DURATION);


}
void  HelloWorld::createSmallEnemy(float dt) {
#pragma region  smallEnemy游戏元素及动画
	auto smallEnemy = Sprite::create("Texture/SmallEnemy1.png");
	this->addChild(smallEnemy);
	smallEnemy->setPosition(Vec2(visibleSize.width / 3 * 2, smallEnemy->getContentSize().height / 5 * 4));
	smallEnemy->setName("smallEnemy");
	Vector<SpriteFrame*>smallEnemyFrames;
	smallEnemyFrames.pushBack(SpriteFrame::create("Texture/SmallEnemy1.png", Rect(0, 0, 71, 73)));
	smallEnemyFrames.pushBack(SpriteFrame::create("Texture/SmallEnemy2.png", Rect(0, 0, 71, 73)));
	smallEnemy->runAction(RepeatForever::create(Animate::create(Animation::createWithSpriteFrames(smallEnemyFrames, 0.3f))));
#pragma endregion

#pragma region SmallEnemy的移动（AI）
	//算法描述:SmallEnemy从屏幕上方出现移动到屏幕下方消失，线性匀速
	//初始随机位置，移动，消失
	auto size = smallEnemy->getContentSize();
	//随机出现在屏幕之外
	auto x = rand() % static_cast<int>(visibleSize.width - size.width) + size.width / 2;
	auto startPos = Vec2(x, visibleSize.height + size.height / 2);
	auto endPos = Vec2(x, -size.height / 2);
	//设置起始位置
	smallEnemy->setPosition(startPos);
	//移动（使用Cocos2d-x的Action的相关类）
	auto moveAct = MoveTo::create(2.5f, endPos);
	//消失动作
	auto callFunc = CallFunc::create([=]() {smallEnemy->removeFromParent();});
	auto action = Sequence::create(moveAct, callFunc, nullptr);
	smallEnemy->runAction(action);
#pragma endregion
#pragma region Hero刚体
	//auto size = hero->getContentSize();
	auto body = PhysicsBody::createBox(size);
	//设置标志
	body->setTag(ContactTag::HERO);
	//设置碰撞掩码
	body->setContactTestBitmask(0xFFFFFFFF);
	smallEnemy->setPhysicsBody(body);
#pragma endregion


}
void  HelloWorld::createBigEnemy(float dt) {
#pragma region  BigEnemy
	auto bigEnemy = Sprite::create("Texture/BigEnemy1.png");
	this->addChild(bigEnemy);
	bigEnemy->setPosition(Vec2(visibleSize.width / 3, bigEnemy->getContentSize().height / 5 * 4));
	bigEnemy->setName("bigEnemy");
	Vector<SpriteFrame*>bigEnemyFrames;
	bigEnemyFrames.pushBack(SpriteFrame::create("Texture/BigEnemy1.png", Rect(0, 0, 83, 105)));
	bigEnemyFrames.pushBack(SpriteFrame::create("Texture/BigEnemy2.png", Rect(0, 0, 83, 105)));
	bigEnemy->runAction(RepeatForever::create(Animate::create(Animation::createWithSpriteFrames(bigEnemyFrames, 0.3f))));
#pragma endregion
#pragma region BigEnmey的移动（AI）
	//算法描述：移动到屏幕上方悬停并左右移动，还具备射击功能
	//出现->移动->悬停移动

	auto size = bigEnemy->getContentSize();
	auto x = rand() % static_cast<int>(visibleSize.width - size.width) + size.width / 2;
	auto startPos = Vec2(x, visibleSize.height + size.height / 2);
	//后期可以考虑悬停的范围可变，即Y坐标随机
	auto endPos = Vec2(x, visibleSize.height / 10 * 9);
	bigEnemy->setPosition(startPos);
	//移动动作
	auto moveAct = MoveTo::create(1.0f, endPos);
	auto shootAct = CallFunc::create([=]() {log("bigEnemy");
	bigEnemy->schedule(schedule_selector(HelloWorld::enemyFire), ENEMY_FIRE_DURATION); });
	auto moveAndBackAct = CallFunc::create([=]() {auto moveToRight = MoveTo::create(3.0f, Vec2(visibleSize.width - size.width / 2, (visibleSize.height / 10 * 9)));
	auto moveToLeft = MoveTo::create(3.0f, Vec2(size.width / 2, visibleSize.height / 10 * 9));
	auto foreverAct = RepeatForever::create(Sequence::create(moveToRight, moveToLeft, nullptr));
	bigEnemy->runAction(foreverAct);
	});
	auto action = Sequence::create(moveAct, shootAct, moveAndBackAct, nullptr);
	bigEnemy->runAction(action);

#pragma endregion
#pragma region Hero刚体
	//auto size = bigEnemy->getContentSize();
	auto body = PhysicsBody::createBox(size);
	//设置标志
	body->setTag(ContactTag::ENEMY);
	//设置碰撞掩码
	body->setContactTestBitmask(0xFFFFFFFF);
	bigEnemy->setPhysicsBody(body);
#pragma endregion
}
void  HelloWorld::heroFire(float dt) {
	auto bullet = Sprite::create("Texture/HeroBullet.png");
	this->addChild(bullet);
	//子弹的定位:Hero的正上方，即Hero精灵的上边界正中
	auto hero = this->getChildByName("Hero");
	auto startPos = hero->getPosition() + Vec2(0,hero->getContentSize().height/2);
	auto endPos = Vec2(hero->getPositionX(), visibleSize.height + bullet->getContentSize().height / 2);
	//动作持续时间
	auto duration = (endPos.y - startPos.y) / HERO_BULLET_SPEED;
	bullet->setPosition(startPos);
	auto fireAct = Sequence::create(MoveTo::create(duration, endPos), CallFunc::create([=]() {SimpleAudioEngine::getInstance()->playEffect("Audio/shoot.wav"); }),
		CallFunc::create([=]() {bullet->removeFromParent(); }),
		nullptr
	);
	bullet->runAction(fireAct);
#pragma region Hero刚体
	auto size = bullet->getContentSize();
	auto body = PhysicsBody::createBox(size);
	//设置标志
	body->setTag(ContactTag::HERO_BULLET);
	//设置碰撞掩码
	body->setContactTestBitmask(0xFFFFFFFF);
	bullet->setPhysicsBody(body);
#pragma endregion
}
void HelloWorld::enemyFire(float dt)
{
	auto bullet = Sprite::create("Texture/EnemyBullet.png");
	//调度器是绑定在bigenmey
	this->getParent()->addChild(bullet);
	//是在bigenmey下沿正中
	auto startPos = this->getPosition() - Vec2(0, this->getContentSize().height / 2);
	//结束位置
	auto endPos = Vec2(this->getPositionX(), -bullet->getContentSize().height / 2);

	auto duration = (startPos.y + endPos.y) / ENEMY_BULLET_SPEED;
	bullet->setPosition(startPos);

	auto  fireAct = Sequence::create(
		MoveTo::create(duration, endPos),
		CallFunc::create([=]() {bullet->removeFromParent(); }),
		nullptr
	);
	bullet->runAction(fireAct);
#pragma region Hero刚体
	auto size = bullet->getContentSize();
	auto body = PhysicsBody::createBox(size);
	//设置标志
	body->setTag(ContactTag::ENEMY_BULLET);
	//设置碰撞掩码
	body->setContactTestBitmask(0xFFFFFFFF);
	bullet->setPhysicsBody(body);
#pragma endregion

}
void HelloWorld::initBackground() {
	auto bg1 = Sprite::create("Texture/background.png");
	bg1->setName("bg1");
	//满屏放置
	bg1->setAnchorPoint(Vec2::ZERO);
	bg1->setPosition(Vec2::ZERO);
	//按屏幕宽度缩放
	auto bgSize = bg1->getContentSize();
	auto xScale = visibleSize.width / bgSize.width;
	bg1->setScaleX(xScale);
	this->addChild(bg1, -100);

	auto bg2 = Sprite::create("Texture/background.png");
	bg2->setName("bg2");
	//满屏放置
	bg2->setAnchorPoint(Vec2::ZERO);
	bg2->setPosition(Vec2(0, bgSize.height));
	bg2->setScaleX(xScale);
	this->addChild(bg2, -100);
	//调度器
	this->schedule(schedule_selector(HelloWorld::moveBackground), 0.01f);

}
void HelloWorld::moveBackground(float dt){
	auto bg1 = this->getChildByName("bg1");
	auto bg2 = this->getChildByName("bg2");
	//接缝位置
	auto bgY = bg1->getContentSize().height;
	if (bg1->getPositionY() < -bgY) { bg1->setPosition(Vec2::ZERO);
	bg2->setPosition(Vec2(0, bgY));

	}
	else
	{
		bg1->setPosition(bg1->getPosition() - Vec2(0.0f, 2.0f));
		bg2->setPosition(bg2->getPosition() - Vec2(0.0f, 2.0f));


		
	}
	

}

