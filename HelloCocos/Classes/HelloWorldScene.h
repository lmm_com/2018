#ifndef __HELLOWORLD_SCENE_H__
#define __HELLOWORLD_SCENE_H__

#include "cocos2d.h"

class HelloWorld : public cocos2d::Layer
{
public:
    // there's no 'id' in cpp, so we recommend returning the class instance pointer
	//初始化场景
    static cocos2d::Scene* createScene();

    // Here's a difference. Method 'init' in cocos2d-x returns bool, instead of returning 'id' in cocos2d-iphone
    virtual bool init();
	virtual void update(float dt);

    // implement the "static create()" method manually
	//实现了创建场景的类方法
    CREATE_FUNC(HelloWorld);
	//菜单处理方法
	void menuCloseCallback(Ref*pSender);
};

#endif // __HELLOWORLD_SCENE_H__
