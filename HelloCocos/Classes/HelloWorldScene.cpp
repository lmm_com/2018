#include "HelloWorldScene.h"
#include "editor-support/cocostudio/CocoStudio.h"
#include "ui/CocosGUI.h"
#include "SimpleAudioEngine.h"
USING_NS_CC;
using namespace CocosDenshion;

using namespace cocostudio::timeline;

Scene* HelloWorld::createScene()
{
    // 'scene' is an autorelease object
	//创建一个游戏场景对象
    auto scene = Scene::create();
    
    // 'layer' is an autorelease object
	//创建游戏层动画
    auto layer = HelloWorld::create();

    // add layer as a child to scene
	//层加入到场景中
    scene->addChild(layer);

    // return the scene
    return scene;
}

// on "init" you need to initialize your instance
bool HelloWorld::init()
{
    /**  you can create scene with following comment code instead of using csb file.
    // 1. super init first
    if ( !Layer::init() )
    {
        return false;
    }
    
    Size visibleSize = Director::getInstance()->getVisibleSize();
    Vec2 origin = Director::getInstance()->getVisibleOrigin();

    /////////////////////////////
    // 2. add a menu item with "X" image, which is clicked to quit the program
    //    you may modify it.

    // add a "close" icon to exit the progress. it's an autorelease object
    auto closeItem = MenuItemImage::create(
                                           "CloseNormal.png",
                                           "CloseSelected.png",
                                           CC_CALLBACK_1(HelloWorld::menuCloseCallback, this));
    
	closeItem->setPosition(Vec2(origin.x + visibleSize.width - closeItem->getContentSize().width/2 ,
                                origin.y + closeItem->getContentSize().height/2));

    // create menu, it's an autorelease object
    auto menu = Menu::create(closeItem, NULL);
    menu->setPosition(Vec2::ZERO);
    this->addChild(menu, 1);

    /////////////////////////////
    // 3. add your codes below...

    // add a label shows "Hello World"
    // create and initialize a label
    
    auto label = Label::createWithTTF("Hello World", "fonts/Marker Felt.ttf", 24);
    
    // position the label on the center of the screen
    label->setPosition(Vec2(origin.x + visibleSize.width/2,
                            origin.y + visibleSize.height - label->getContentSize().height));

    // add the label as a child to this layer
    this->addChild(label, 1);

    // add "HelloWorld" splash screen"
    auto sprite = Sprite::create("HelloWorld.png");

    // position the sprite on the center of the screen
    sprite->setPosition(Vec2(visibleSize.width/2 + origin.x, visibleSize.height/2 + origin.y));

    // add the sprite as a child to this layer
    this->addChild(sprite, 0);
    **/
    
    //////////////////////////////
    // 1. super init first
	//初始化层
    if ( !Layer::init() )
    {
        return false;
    }
	Size visibleSize = Director::getInstance()->getVisibleSize();
	//获取视窗原点
	Vec2 origin = Director::getInstance()->getVisibleOrigin();

#pragma region 文字内容显示
	//auto rootNode = CSLoader::createNode("MainScene.csb");

	//addChild(rootNode);
	//文字显示 TTF
	auto labelWithTTF = Label::createWithTTF("Hello Cocos", "font/Marker Felt.ttf", 24);
	labelWithTTF->setPosition(origin.x + visibleSize.width / 2, origin.y + visibleSize.height - labelWithTTF->getContentSize().height);
	//labelWithTTF->enableOutline
	
	labelWithTTF->setTag(123);
	labelWithTTF->setName("str_hello_cocos");
	this->addChild(labelWithTTF, 10);
	//文字显示 BMFont

	//创建所需要的字典类
	auto dict = __Dictionary::createWithContentsOfFile("font/string.xml");
	auto strStart = dynamic_cast<__String*>(dict->objectForKey("start"));//中英文切换
	auto labelWithBMFont = Label::createWithBMFont("font/game.fnt", strStart->getCString());
	labelWithBMFont->setPosition(Vec2(origin.x + labelWithBMFont->getContentSize().width, 240.0f));
	labelWithBMFont->enableShadow(Color4B::BLUE, Size(5, -5), 1);
	this->addChild(labelWithBMFont, 20);


#pragma endregion 
#pragma region 精灵
	//2.产生精灵对象
	auto sprite = Sprite::create("HelloWorld.png");
	//设置位置参数,默认锚点为中央
	sprite->setPosition(Vec2(visibleSize.width / 2 + origin.x, visibleSize.height / 2 + origin.y));
	//3,加入层中
	this->addChild(sprite, 0);
	auto spriteRect = Sprite::create("HelloWorld.png", Rect(20, 30, 100, 100));
	spriteRect->setPosition(Vec2(200.0f, 200.0f));
	this->addChild(spriteRect);
	//纹理缓存：提高利用率，提高访问速度。在Cocos2d的一个实例中只存在一个缓冲区
	auto textureCache = Director::getInstance()->getTextureCache()->addImage("HelloWorld.png");
	auto spriteWithTexture = Sprite::create();
	spriteWithTexture->setTextureRect(Rect(300, 400, 100, 100));
	spriteWithTexture->setPosition(Vec2(10.0f, 10.0f));
	this->addChild(spriteWithTexture);
	//精灵帧缓存
	SpriteFrameCache::getInstance()->addSpriteFramesWithFile("blueman.plist");
	auto blueman = Sprite::createWithSpriteFrameName("01.png");
	blueman->setPosition(Vec2(800.0f, 200.0f));
	this->addChild(blueman);

#pragma endregion 图片文件/图片文件+区域/精灵帧缓存
#pragma region 精灵菜单类
	auto closeMenuItem = MenuItemImage::create("close_normal.png", "close_selected.png", CC_CALLBACK_1(HelloWorld::menuCloseCallback, this));
	closeMenuItem->setPosition(Vec2(origin.x + visibleSize.width
		- closeMenuItem->getContentSize().width / 2, origin.y + closeMenuItem->getContentSize().height / 2));
	auto menu = Menu::create(closeMenuItem, NULL);
	menu->setPosition(Vec2::ZERO);
	this->addChild(menu);



#pragma endregion
#pragma region 动作（Action）
	//blueman->runAction(MoveBy::create(2, Vec2(50, 50)));
	//贝塞尔曲线
	ccBezierConfig bezier;
	bezier.controlPoint_1 = Vec2(0, visibleSize.height / 2);
	bezier.controlPoint_2 = Vec2(300,-visibleSize.height / 2);
	bezier.endPosition = Vec2(300, 100);
	blueman->runAction(RepeatForever::create(Spawn::create(BezierTo::create(4.0, bezier), RotateBy::create(3.0, 180),
		NULL)));
	//精灵帧动画
	Vector<SpriteFrame*>animationFrames;
	char str[100] = { 0 };
	for (int i=1; i < 8; ++i) {
		sprintf(str, "0%d.png", i);
		auto frame = SpriteFrameCache::getInstance()->getSpriteFrameByName(str);
		animationFrames.pushBack(frame);

	}
	auto animation = Animation::createWithSpriteFrames(animationFrames, 0.3f);
	blueman->runAction(Animate::create(animation));

#pragma endregion 重复（贝塞尔曲线）/精灵帧动画
#pragma region 鼠标事件处理
	auto mouseListener = EventListenerMouse::create();
	mouseListener->onMouseMove = [](Event*event)
	{
		auto e = dynamic_cast<EventMouse*>(event);
		log("x:%f,Y:%f", e->getCursorX(), e->getCursorY());

	};
	mouseListener->onMouseDown = [](EventMouse*event) {
		log("Mouse Key:%i", event->getMouseButton());

	};
	//注册事件
	_eventDispatcher->addEventListenerWithSceneGraphPriority(mouseListener,this);
#pragma endregion
#pragma region 音频播放
	auto audioEngine = SimpleAudioEngine::getInstance();
	//预加载音频资源到缓冲区
	audioEngine->preloadBackgroundMusic("sound/1.mp3");
	audioEngine->preloadEffect("effect.mp3");

	audioEngine->playBackgroundMusic("sound/1.mp3");
	audioEngine->playEffect("effect.mp3");
	//调整音量
	audioEngine->setBackgroundMusicVolume(0.1f);
	audioEngine->setEffectsVolume(0.5f);

#pragma endregion 音乐与音效
#pragma region 粒子系统
	auto particlleDemo = ParticleFlower::create();
	particlleDemo->setPosition(320, 240);
	this->addChild(particlleDemo);
	auto myParticle = ParticleSystemQuad::create();
#pragma endregion
#pragma region 定时器控制
	//this->scheduleUpdate();
	this->schedule(schedule_selector(HelloWorld::update), 1.0f / 60);
#pragma endregion

  
    return true;
}
void HelloWorld::update(float dt) {
	auto labelWithTTF = this->getChildByTag(123);
	
	labelWithTTF->setPosition(labelWithTTF->getPosition() + Vec2(2, -2));
}
void HelloWorld::menuCloseCallback(Ref * pSender)
{
	//关闭游戏
	//停止计时器
	unscheduleUpdate();
	unscheduleAllCallbacks();
	//结束
	Director::getInstance()->end();
#if(CC_TARGET_PLATFORM==CC_PLATFORM_IOS)
	exit(0);

#endif 

}
