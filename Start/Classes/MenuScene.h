#ifndef _MENUSCENE_H_
#define _MENUSCENE_H_
#include "cocos2d.h"
USING_NS_CC;

class MenuScene :public Layer
{
public:

	static Scene* createScene();
	virtual bool init();


	CREATE_FUNC(MenuScene);

	void menuStartCallback(Ref* pSender);

	void menuCloseCallback(Ref* pSender);

	void menuBeautiCallback(Ref* pSender);
	//���̼����ص�����
	void onKeyPressed(EventKeyboard::KeyCode keyCode, Event* event);

	void onKeyReleased(EventKeyboard::KeyCode keyCode, Event* event);

	Label*					m_QuitLabel;
	bool					m_isPressQuit;
};




#endif