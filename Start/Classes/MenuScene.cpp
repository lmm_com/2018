#include "MenuScene.h"
#include "GameScene.h"
#include "ZsnakeGameScene.h"
//--------------------------------------------------------------------------------------
Scene* MenuScene::createScene()
{
	auto scene = Scene::create();
	auto layer = MenuScene::create();
	scene->addChild(layer);

	return scene;
}
//--------------------------------------------------------------------------------------
bool MenuScene::init()
{
	if ( !Layer::init() )
	{
		return false;
	}
	m_isPressQuit = false;
	Size ScreenSize = Director::getInstance()->getVisibleSize();
	//通过词典读取文件XML文件
	Dictionary* readFile = Dictionary::createWithContentsOfFile("Chinese.xml");
	//通过key值读取相应的string
	const char* start = ((String*)readFile->objectForKey("Start"))->getCString();
	std::string quit = ((String*)readFile->objectForKey("Quit"))->getCString();
	const char* isQuit = ((String*)readFile->objectForKey("SureQuit"))->getCString();
	const char* Beautiful = ((String*)readFile->objectForKey("Beautiful"))->getCString();

	auto Beaut = MenuItemLabel::create(Label::create(Beautiful, "", 48),
		CC_CALLBACK_1(MenuScene::menuBeautiCallback, this));
	Beaut->setPosition(Vec2(ScreenSize.width / 2, ScreenSize.height / 2 + 200));

	auto startMenu = MenuItemLabel::create(Label::create(start, "fonts\arial", 48),
											CC_CALLBACK_1(MenuScene::menuStartCallback,this));
	startMenu->setPosition(Vec2(ScreenSize.width / 2, ScreenSize.height / 2));
	startMenu->setTag(8);
	auto quitMenu = MenuItemLabel::create(Label::createWithSystemFont(quit, "Segoe Script", 48),
		CC_CALLBACK_1(MenuScene::menuCloseCallback,this));
	quitMenu->setPosition(Vec2(ScreenSize.width / 2, ScreenSize.height / 2 - 100));
	auto menu = Menu::create(startMenu, quitMenu, Beaut, nullptr);
	menu->setPosition(Vec2::ZERO);
	this->addChild(menu);

	m_QuitLabel = Label::create(isQuit, "fonts\arial", 48);
	m_QuitLabel->setPosition(Vec2(ScreenSize.width / 2, ScreenSize.height / 2 + 100));
	m_QuitLabel->setVisible(false);
	this->addChild(m_QuitLabel);
	//注册键盘监听
	auto listener = EventListenerKeyboard::create();
	listener->onKeyPressed = CC_CALLBACK_2(MenuScene::onKeyPressed, this);
	listener->onKeyReleased = CC_CALLBACK_2(MenuScene::onKeyReleased, this);
	_eventDispatcher->addEventListenerWithSceneGraphPriority(listener, this);


}
//--------------------------------------------------------------------------------------
void MenuScene::menuStartCallback(Ref* pSender)
{
	Director::getInstance()->replaceScene(GameScene::createScene());
}
//--------------------------------------------------------------------------------------
void MenuScene::menuCloseCallback(Ref* pSender)
{
	Director::getInstance()->end();
}
//--------------------------------------------------------------------------------------
void MenuScene::menuBeautiCallback(Ref* pSender)
{
	//Director::getInstance()->replaceScene()
}
//--------------------------------------------------------------------------------------
void MenuScene::onKeyPressed(EventKeyboard::KeyCode keyCode, Event* event)
{

}
//--------------------------------------------------------------------------------------
void MenuScene::onKeyReleased(EventKeyboard::KeyCode keyCode, Event* event)
{
	switch (keyCode)
	{
	case cocos2d::EventKeyboard::KeyCode::KEY_ESCAPE:					//安卓手机返回键
	case cocos2d::EventKeyboard::KeyCode::KEY_BACKSPACE:				//笔记本返回键
	{
		if (!m_isPressQuit)
		{
			m_isPressQuit = true;
			m_QuitLabel->setVisible(true);
			auto fad = FadeOut::create(1.0f);
			auto fadin = FadeIn::create(0.1f);
			//延迟一秒
			auto dt = DelayTime::create(1.0f);
			//一秒钟没按返回键 m_isPressQuit = false
			auto callFunc = CallFunc::create([&](){
				m_isPressQuit = false;
				m_QuitLabel->setVisible(false);
			});
			m_QuitLabel->runAction(Sequence::create(fad, dt, callFunc, fadin, nullptr));
		}
		else
		{
			Director::getInstance()->end();
		}
	}
		break;
	case cocos2d::EventKeyboard::KeyCode::KEY_MENU:
	{
	
	}
		break;
	default:
		break;
	}
}
//--------------------------------------------------------------------------------------