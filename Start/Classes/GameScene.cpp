#include "GameScene.h"
#include "MenuScene.h"
//------------------------------------------------------------------------------------------------
GameScene::GameScene()
	:g_isAction(false)
	, g_isFillStar(false)
	, g_StartClickStar(NULL)
	, g_EndClickStar(NULL)
	, g_isEnableTouch(true)
	, g_isMoveTouch(false)
	, g_StartTouch(Vec2::ZERO)
{
	for (int i = 0; i < MAX_ROW_NUM;++i)
	{
		for (int j = 0; j < MAX_COL_NUM;++j)
		{
			g_TotalStar[i][j] = NULL;
		}
	}
}
//------------------------------------------------------------------------------------------------
GameScene::~GameScene()
{

}
//------------------------------------------------------------------------------------------------
Scene* GameScene::createScene()
{
	auto scene = Scene::create();
	auto layer = GameScene::create();
	scene->addChild(layer);

	return scene;
}
//------------------------------------------------------------------------------------------------
bool GameScene::init()
{
	if ( !Layer::init() )
	{
		return false;
	}
	w = 1;
	Size winSize = Director::getInstance()->getVisibleSize();
	CCLOG("winSize.x = %f,winSize.y = %f", winSize.width,winSize.height);
	//创建星星
	initStar();

	auto back = MenuItemLabel::create(Label::create("Back", "", 48), CC_CALLBACK_1(GameScene::meunBackCallback,this));
	back->setPosition(Vec2(back->getContentSize().width/2, SCREEN_HEIGHT-back->getContentSize().height));
	auto menu = Menu::create(back, nullptr);
	menu->setPosition(Vec2::ZERO);
	this->addChild(menu);


	//启动定时器
	this->scheduleUpdate();

	//this->setTouchEnabled(true);
	//注册单点触屏监听器
	auto listener = EventListenerTouchOneByOne::create();//创建监听器
	//监听器绑定回调函数
	listener->onTouchBegan = CC_CALLBACK_2(GameScene::onTouchBegan, this);
	listener->onTouchMoved = CC_CALLBACK_2(GameScene::onTouchMoved, this);
	listener->onTouchEnded = CC_CALLBACK_2(GameScene::onTouchEnded, this);
	//注册监听器
// 	Director::getInstance()->getEventDispatcher()->
// 		addEventListenerWithSceneGraphPriority(listener, this);
	_eventDispatcher->addEventListenerWithSceneGraphPriority(listener, this);

	return true;
}
//------------------------------------------------------------------------------------------------
void GameScene::initStar()
{
	for (int i = 0; i < MAX_ROW_NUM; ++i)
	{
		for (int j = 0; j < MAX_COL_NUM; ++j)
		{
			createGameStar(tagStarPosition(Vec2(i,j)));
		}
	}
	//判断有没有能被消除的星星
	if (isThreeBorder()|| isDead())
	{
		for (int i = 0; i < MAX_ROW_NUM; ++i)
		{
			for (int j = 0; j < MAX_COL_NUM; ++j)
			{
				g_TotalStar[i][j]->removeFromParent();
			}
		}
		initStar();
		return;
	}

}
//------------------------------------------------------------------------------------------------
void GameScene::createGameStar(tagStarPos Vpos)
{
	g_isEnableTouch = false;
	g_isAction = true;
	auto star = Star::createStar(Vpos);
	//star->setAnchorPoint(Vec2(0, 0));	//锚点
	//star->setPosition(StarPosition(Vpos));//设置位置
	//星星初始位置
	Vec2 startPos = Vec2(StarPosition(Vpos).x, StarPosition(Vpos).y + SCREEN_HEIGHT);
	star->setPosition(startPos);
	//星星最后的位置
	Vec2 endPos = StarPosition(Vpos);
	float fSpeed = star->getPositionY() / (1.5 * SCREEN_HEIGHT);
	//创建MoveTo动作
	auto moveTo = MoveTo::create(fSpeed, endPos);
	//lambda方法
	auto callFunC = CallFunc::create([&](){
		g_isAction = false;
		g_isEnableTouch = true;
	});
	//星星执行组合动作
	star->runAction(Sequence::create(moveTo,callFunC,NULL));

	this->addChild(star);
	//记录到数组
	g_TotalStar[Vpos.starRow][Vpos.starCol] = star;
}
//------------------------------------------------------------------------------------------------
Vec2 GameScene::StarPosition(tagStarPos Vpos)
{
	//将行号列号转换成坐标值
	float width = BIANJIE + Vpos.starCol*SPRITE_WIDTH + Vpos.starCol*BOADER_WIDTH;
	float height = XIABIANJIE + Vpos.starRow*SPRITE_WIDTH + Vpos.starRow *BOADER_WIDTH;

	return Vec2(width, height);
}
//------------------------------------------------------------------------------------------------
tagStarPos GameScene::tagStarPosition(Vec2 vec)
{
	tagStarPos tagPos;
	tagPos.starRow = vec.x;
	tagPos.starCol = vec.y;

	return tagPos;
}
void GameScene::meunBackCallback(Ref* pSender)
{
	Director::getInstance()->replaceScene(MenuScene::createScene());
}
//------------------------------------------------------------------------------------------------
void GameScene::update(float delta)
{
	//判断是否正在下落
// 	if ( g_isAction )
// 	{
// 		g_isAction = false;
// 		for (int i = 0; i < MAX_ROW_NUM;++i)
// 		{
// 			for (int j = 0; j < MAX_COL_NUM;++j)
// 			{
// 				if ( g_TotalStar[i][j] && g_TotalStar[i][j]->getNumberOfRunningActions >=1)
// 				{
// 					g_isAction = false;
// 				}
// 			}
// 		}
// 	}
	if (!g_isAction)
	{
		g_isEnableTouch = true;
		if ( g_isFillStar )
		{
			//填充星星
			FillStar();
			g_isFillStar = false;
		}
		else
		{
			//检查并移除星星
			CheckAndReMoveStar();
		}
	}
	
}
//------------------------------------------------------------------------------------------------
bool GameScene::isThreeBorder()
{
	Star*  star = NULL;
	for (int i = 0; i < MAX_ROW_NUM; ++i)
	{
		for (int j = 0; j < MAX_COL_NUM; ++j)
		{
			star = g_TotalStar[i][j];
			if ( !star )
			{
				continue;
			}
			//先判断左右三个相同
			Vector<Star*>  LR_Star;
			CheckRow(star, LR_Star);
			if ( LR_Star.size() >= 3 )
			{
				return false;
			}
			//先判断上下三个相同
			Vector<Star*> UD_Star;
			CheckCoL(star, UD_Star);
			if (UD_Star.size() >= 3)
			{
				return false;
			}
		}
	}
	return true;
}
//------------------------------------------------------------------------------------------------
void GameScene::CheckRow(Star* s, Vector<Star*> &vec)
{
	//xu:其实并不用往左找相邻并相同的星星，因为循环是从小值到大值，那么就是从左到右的顺序
	vec.pushBack(s);
	//得到星星的行号
	int row = s->getStarPos().starRow;
	int col = s->getStarPos().starCol;

	int  leftCol = col, rightCol = col;

	//判断右
	while (leftCol < MAX_COL_NUM - 1)
	{
		Star*  star = g_TotalStar[row][leftCol + 1];

		//判断同行相邻星星颜色是否相同
		if (star && s->getStarColor() == star->getStarColor())
		{
			//颜色一样就把他放入Vector里
			vec.pushBack(star);
			leftCol++;
		}
		else
		{
			break;
		}
	}

	//判断左
	while ( rightCol >= 1 )
	{
		Star*  star = g_TotalStar[row][rightCol - 1];
		if (star && s->getStarColor() == star->getStarColor())
		{
			vec.pushBack(star);
			rightCol--;
		}
		else
		{
			break;
		}
	}
}
//------------------------------------------------------------------------------------------------
void GameScene::CheckCoL(Star* s, Vector<Star*> &vec)
{
	//xu:同行找法一样的道理，循环从小值到大值就是从下到上的，所以不用再往下找
	vec.pushBack(s);
	//得到行和列
	int row = s->getStarPos().starRow;
	int col = s->getStarPos().starCol;
	int upRow = row,downRow = row;

	//向上判断
	while (upRow < MAX_ROW_NUM - 1)
	{
		Star* star = g_TotalStar[upRow + 1][col];
		if (star && s->getStarColor() == star->getStarColor())
		{
			vec.pushBack(star);
			upRow++;
		}
		else
		{
			break;
		}
	}
	//向下判断
	while ( downRow >= 1 )
	{
		Star* star = g_TotalStar[downRow - 1][col];
		if (star && s->getStarColor() == star->getStarColor())
		{
			vec.pushBack(star);
			downRow--;
		}
		else
		{
			break;
		}
	}
}
//------------------------------------------------------------------------------------------------
bool GameScene::isDead()
{
	//判断有没有滑动能消除的块
	Star* star = NULL;
	for (int i = 0; i < MAX_ROW_NUM;++i)
	{
		for (int j = 0; j < MAX_COL_NUM;++j)
		{
			star = g_TotalStar[i][j];
			if (  !star )
			{
				continue;
			}
			//相邻两个颜色相同（行号相同列号不同）
			//由于循环是从小到大的值，所有不用往左边进行找相邻颜色相同的，这样造成重复。
			if ( j+1< MAX_COL_NUM )
			{
				if (star->getStarColor() == g_TotalStar[i][j + 1]->getStarColor())
				{
					//找右边三个
					if (i + 1 < MAX_ROW_NUM && j + 2 < MAX_COL_NUM &&
						star->getStarColor() == g_TotalStar[i + 1][j + 2]->getStarColor())
					{
						//右上角星星
						return false;
					}
					if (i - 1 >= 0 && j + 2 < MAX_COL_NUM &&
						star->getStarColor() == g_TotalStar[i - 1][j + 2]->getStarColor())
					{
						//右下角星星
						return false;
					}
					if (j + 3 < MAX_COL_NUM&&
						star->getStarColor() == g_TotalStar[i][j + 3]->getStarColor())
					{
						//右边间隔两个的星星
						return false;
					}
					//找左边三个
					if (i + 1 < MAX_ROW_NUM&&j - 1 >= 0 &&
						star->getStarColor() == g_TotalStar[i + 1][j - 1]->getStarColor())
					{
						//左上角
						return false;
					}
					if (i - 1 >= 0 && j - 1 >= 0 && 
						star->getStarColor() == g_TotalStar[i - 1][j - 1]->getStarColor())
					{
						//左下角
						return false;
					}
					if (j - 2 >= 0 && star->getStarColor() == g_TotalStar[i][j - 2]->getStarColor())
					{
						//左边间隔一个的星星
						return false;
					}
				}
			}
			//相邻两个颜色相同（列号相同行号不同）
			//由于循环是从小到大的值，所有不用往下边进行找相邻颜色相同的，这样造成重复。
			if (i + 1 < MAX_ROW_NUM)
			{
				if ( star->getStarColor() == g_TotalStar[i+1][j]->getStarColor())
				{
					//正上方间隔两个的位置
					if (i + 3 < MAX_ROW_NUM)
					{
						if (star->getStarColor() == g_TotalStar[i + 3][j]->getStarColor())
						{
							return false;
						}
					}
					//正上右边
					if (i + 2 < MAX_ROW_NUM&&j + 1 < MAX_COL_NUM&&
						star->getStarColor() == g_TotalStar[i + 2][j + 1]->getStarColor())
					{
						return false;
					}
					//正上左边
					if (i + 2 < MAX_ROW_NUM&&j - 1 >= 0 && 
						star->getStarColor() == g_TotalStar[i + 2][j - 1]->getStarColor())
					{
						return false;
					}
					//正下方
					if (i - 2 >= 0 && 
						star->getStarColor() == g_TotalStar[i - 2][j]->getStarColor())
					{
						return false;
					}
					//正下方右边
					if (i - 1 >= 0 && j + 1 < MAX_COL_NUM&&
						star->getStarColor() == g_TotalStar[i -1][j + 1]->getStarColor())
					{
						return false;
					}
					//正下方左边
					if (i - 1 >= 0 && j - 1 >= 0 && 
						star->getStarColor() == g_TotalStar[i - 1][j - 1]->getStarColor())
					{
						return  false;
					}
				}
			}

			//中间间隔一个的两个颜色相同(行号相同列号不同)
			if (j + 2 < MAX_COL_NUM)
			{
				if (star->getStarColor() == g_TotalStar[i][j + 2]->getStarColor())
				{
					//中间上面位置星星颜色
					if (i + 1 < MAX_ROW_NUM&&j + 1 < MAX_COL_NUM&&
						star->getStarColor() == g_TotalStar[i + 1][j + 1]->getStarColor())
					{
						return false;
					}
					//中间下方位置星星颜色
					if (i - 1 >= 0 && j + 1 < MAX_COL_NUM&&
						star->getStarColor() == g_TotalStar[i - 1][j + 1]->getStarColor())
					{
						return false;
					}

				}
			}
			//中间间隔一个的两个颜色相同(列号相同行号不同)
			if (i + 2 < MAX_ROW_NUM)
			{
				if (star->getStarColor() == g_TotalStar[i + 2][j]->getStarColor())
				{
					//中间右边位置星星
					if (i + 1 < MAX_ROW_NUM&&j + 1 < MAX_COL_NUM&& 
						star->getStarColor() == g_TotalStar[i + 1][j + 1]->getStarColor())
					{
						return false;
					}
					//中间左边位置星星
					if (i + 1 < MAX_ROW_NUM&&j - 1 >= 0 && 
						star->getStarColor() == g_TotalStar[i + 1][j - 1]->getStarColor())
					{
						return  false;
					}

				}
			}
		}
	}
	return  true;
}
//------------------------------------------------------------------------------------------------
void GameScene::CheckAndReMoveStar()
{
	Star*  star = NULL;
	for (int i = 0; i < MAX_ROW_NUM; ++i)
	{
		for (int j = 0; j < MAX_COL_NUM; ++j)
		{
			star = g_TotalStar[i][j];
			if ( !star  )
			{
				continue;
			}
			if ( star->getEnableRemove() )
			{
				continue;
			}
			//行判断
			Vector<Star*>    LR_vec;
			CheckRow(star, LR_vec);
			//列判断
			Vector<Star*>	 UD_vec;
			CheckCoL(star, UD_vec);
// 			//判断链表长度
// 			if (LR_vec.size() >= 3 && UD_vec.size() >= 3)
// 			{
// 			
// 			}
			//判断哪个容器更长
			Vector<Star*> Max_vec = (LR_vec.size()>UD_vec.size()) ? LR_vec : UD_vec;
			//标记为能被消除
			if (Max_vec.size() >= 3)
			{
				for (auto vc:Max_vec)
				{
					if ( !vc  )
					{
						continue;
					}
					if ( vc->getEnableRemove() )
					{
						continue;
					}
					MarkReMoveStar(vc);
				}
				//迭代器方法
// 				for (Vector<Star*>::iterator iter = Max_vec.begin(); 
// 					iter!=Max_vec.end(); iter++)
// 				{
// 					star = (Star*)*iter;
// 					if (!star)
// 					{
// 						continue;
// 					}
// 
// 					MarkReMoveStar(star);
// 				}
// 				for (int L = 0; L < Max_vec.size(); ++L)
// 				{
// 					if (!Max_vec.at(L))
// 					{
// 						continue;
// 					}
// 					if (Max_vec.at(L)->getEnableRemove())
// 					{
// 						continue;
// 					}
// 					//标记星星
// 					MarkReMoveStar(Max_vec.at(L));
// 				}
			}
			
		}
	}

	//消除星星
	RemoveStar();

}
//------------------------------------------------------------------------------------------------
void GameScene::MarkReMoveStar(Star* star)
{
	//标记星星能否被消除
	if ( !star->getEnableRemove() )
	{
		star->setEnableRemove(true);
	}
	else
	{
		return;
	}
}
//------------------------------------------------------------------------------------------------
void GameScene::RemoveStar()
{
	Star*  star = NULL;
	for (int i = 0; i < MAX_ROW_NUM;++i)
	{
		for (int j = 0; j < MAX_COL_NUM;++j)
		{
			star = g_TotalStar[i][j];
			if (!star)
			{
				continue;
			}
			//如果被标记移除的话那么我们就删除该对象
			if (star->getEnableRemove())
			{
				g_isEnableTouch = false;
//				g_isFillStar = true;
// 				star->removeFromParent()
				auto scale = ScaleTo::create(0.5f, 0.01);
				//执行动作并删除对象
				star->runAction(Sequence::create(scale,
					CallFuncN::create(CC_CALLBACK_1(GameScene::RemoveStarAction, this)), NULL));
			}
		}
	}
}
//------------------------------------------------------------------------------------------------
void GameScene::RemoveStarAction(Node* node)
{
	Star* star = (Star*)node;
	//删除对象
	star->removeFromParent();
	g_TotalStar[star->getStarPos().starRow][star->getStarPos().starCol] = NULL;
	g_isFillStar = true;
	g_isEnableTouch = true;
}
//------------------------------------------------------------------------------------------------
void GameScene::FillStar()
{
	g_isEnableTouch = false;

	//记录每列空位置的总数-
	int NeedFileStarCol[MAX_COL_NUM] = { 0 };
	int NeedFileStar = 0;				//记录每列移除掉星星的空位
	Star* star = NULL;
	//先找列后找行
	for (int col = 0; col < MAX_COL_NUM; ++col)
	{
		NeedFileStar = 0;
		for (int row = 0; row < MAX_ROW_NUM; ++row)
		{
			star = g_TotalStar[row][col];
			if ( !star )
			{
				NeedFileStar++;
			}
			else
			{
				if ( NeedFileStar > 0 )
				{
					//新的行号
					int NewRow = row - NeedFileStar;

					g_TotalStar[NewRow][col] = star;
					g_TotalStar[row][col] = NULL;

					//开始的位置
					Vec2 statePos = star->getPosition();
					//最终位置
					Vec2 endPos = StarPosition(tagStarPosition(Vec2(NewRow, col)));
					//下落速度
					float fSpeed = statePos.y / (1.5*SCREEN_HEIGHT);
					//执行动作
					g_TotalStar[NewRow][col]->runAction(
						MoveTo::create(fSpeed, endPos));
					//更新移动对象的行和列
					g_TotalStar[NewRow][col]->setStarPos(tagStarPosition(Vec2(NewRow, col)));

				}
			}
		}
		NeedFileStarCol[col] = NeedFileStar;
	}
	//补充的方法
	//方法一
	for (int col = 0; col < MAX_COL_NUM; ++col)
	{
		for (int row = MAX_ROW_NUM - NeedFileStarCol[col]; row < MAX_ROW_NUM; ++row)
		{
			createGameStar(tagStarPosition(Vec2(row, col)));
		}
	}
	
	//方法二
// 	for (int i = 0; i < MAX_ROW_NUM;++i)
// 	{
// 		for (int j = 0; j < MAX_COL_NUM;++j)
// 		{
// 			if ( g_TotalStar[i][j] ==NULL )
// 			{
// 				 
// 				createGameStar(tagStarPosition(Vec2(i,j)));
// 			}
// 		}
// 	}
	//方法三
// 	for (int col = 0; col < MAX_COL_NUM; ++col)
// 	{
// 		for (int row = MAX_ROW_NUM; row > 0; --row)
// 		{
// 			if ( g_TotalStar[row][col] == NULL )
// 			{
// 				createGameStar(tagStarPosition(Vec2(row, col)));
// 			}
// 			else
// 			{
// 				break;
// 			}
// 		}
// 	}
}
//------------------------------------------------------------------------------------------------
bool GameScene::onTouchBegan(Touch *touch, Event *unused_event)
{
	if ( !g_isEnableTouch )
	{
		return false;
	}
	g_StartTouch = touch->getLocation();//得到是基于3D的单击坐标 OpenGL
	//限制触摸的范围
	if (g_StartTouch.x >= BIANJIE - SPRITE_WIDTH / 2 &&
		g_StartTouch.x <= BIANJIE - SPRITE_WIDTH / 2 + MAX_COL_NUM*SPRITE_WIDTH + BOADER_WIDTH*(MAX_COL_NUM - 1)
		&& g_StartTouch.y >= XIABIANJIE - SPRITE_WIDTH / 2 &&
		g_StartTouch.y <= XIABIANJIE - SPRITE_WIDTH / 2 + SPRITE_WIDTH / 2 + MAX_ROW_NUM*SPRITE_WIDTH + BOADER_WIDTH*(MAX_ROW_NUM - 1))
	{
		//得到触摸的星星
		g_StartClickStar = touchStar(g_StartTouch);

		if (g_StartClickStar && g_EndClickStar && isNearStar())
		{
			//两个星星进行交换
			CCLOG("liang ge xingxing jiaohuan!");
			swapStar();
		}
	}

	return true;
}
//------------------------------------------------------------------------------------------------
void GameScene::onTouchMoved(Touch *touch, Event *unused_event)
{
	//滑动的坐标
	Vec2 movePos = touch->getLocation();
	//通过点击的坐标和移动的坐标
	if (!g_isMoveTouch &&
		(abs(movePos.x - g_StartTouch.x) >= SPRITE_WIDTH / 2 ||
		abs(movePos.y - g_StartTouch.y) >= SPRITE_WIDTH / 2))
	{
		g_isMoveTouch = true;
		//判断滑动方向
		int Row = g_StartClickStar->getStarPos().starRow;
		int Col = g_StartClickStar->getStarPos().starCol;

		//右边
		Rect  LeftRect = Rect(g_StartClickStar->getPositionX() + SPRITE_WIDTH / 2,
			g_StartClickStar->getPositionY() - SPRITE_WIDTH / 2,
			SPRITE_WIDTH, SPRITE_WIDTH);
		//设置不超过右边界

		if (LeftRect.containsPoint(movePos))
		{
			if (Col + 1 < MAX_COL_NUM)
			{
				g_EndClickStar = g_TotalStar[Row][Col + 1];
				swapStar();
				return;
			}
		}
		//左边
		Rect RightRect = Rect(g_StartClickStar->getPositionX() - SPRITE_WIDTH - SPRITE_WIDTH / 2,
			g_StartClickStar->getPositionY() - SPRITE_WIDTH / 2,
			SPRITE_WIDTH,SPRITE_WIDTH);
		//设置不超过边界

		if ( RightRect.containsPoint(movePos) )
		{
			if (Col - 1 >= 0)
			{
				g_EndClickStar = g_TotalStar[Row][Col - 1];
				swapStar();
				return;
			}
		}
		//上边
		Rect UpRect = Rect(g_StartClickStar->getPositionX()-SPRITE_WIDTH/2,
			g_StartClickStar->getPositionY()+SPRITE_WIDTH/2,
			SPRITE_WIDTH,SPRITE_WIDTH);

		if ( UpRect.containsPoint(movePos) )
		{
			if (Row + 1 < MAX_ROW_NUM)
			{
				g_EndClickStar = g_TotalStar[Row+1][Col];
				swapStar();
				return;
			}
		}
		//下边
		Rect DownRect = Rect(g_StartClickStar->getPositionX()-SPRITE_WIDTH/2,
			g_StartClickStar->getPositionY()-SPRITE_WIDTH-SPRITE_WIDTH/2,
			SPRITE_WIDTH,SPRITE_WIDTH);

		if (DownRect.containsPoint(movePos))
		{
			if (Row - 1 >= 0)
			{
				g_EndClickStar = g_TotalStar[Row - 1][Col];
				swapStar();
				return;
			}
		}

	}

}
//------------------------------------------------------------------------------------------------
void GameScene::onTouchEnded(Touch *touch, Event *unused_event)
{
	//当点击的星星不存在，或两个星星相邻时不记录星星
	if (!g_StartClickStar || isNearStar() || g_isMoveTouch)
	{
		g_EndClickStar = NULL;
		g_isMoveTouch = false;
	}
	else
	{
		g_EndClickStar = g_StartClickStar;
	}
}
//------------------------------------------------------------------------------------------------
Star* GameScene::touchStar(Vec2 vPos)
{
	Star*  star = NULL;
	//星星的高和宽
	Size size = Size(SPRITE_WIDTH, SPRITE_WIDTH);
	Vec2 vStarPos = Vec2::ZERO;			//星星的左下角坐标
	//通过左下角坐标和星星的高宽得到矩阵
	Rect rect = Rect(vStarPos, size);
	for (int r = 0; r < MAX_ROW_NUM; r++)
	{
		for (int c = 0; c < MAX_COL_NUM; c++)
		{
			star = g_TotalStar[r][c];
			if ( !star )
			{
				continue;
			}
			//行跟列得到星星的左下角坐标
			vStarPos.x = star->getPositionX() - SPRITE_WIDTH / 2;
			vStarPos.y = star->getPositionY() - SPRITE_WIDTH / 2;
			rect = Rect(vStarPos, size);
			if ( rect.containsPoint(vPos) )
			{
				CCLOG("star is have = %d,%d",r,c);
				return star;
			}
		}
	}
	return NULL;
}
//------------------------------------------------------------------------------------------------
bool GameScene::isNearStar()
{
	if ( !g_StartClickStar||!g_EndClickStar )
	{
		return false;
	}
	//如果两个星星行相同的话
	if (g_StartClickStar->getStarPos().starRow == g_EndClickStar->getStarPos().starRow)
	{
		if (g_StartClickStar->getStarPos().starCol - g_EndClickStar->getStarPos().starCol ==1||
			g_EndClickStar->getStarPos().starCol - g_StartClickStar->getStarPos().starCol == 1)
		{
			return true;
		}
	}
	//列相同
	if (g_StartClickStar->getStarPos().starCol == g_EndClickStar->getStarPos().starCol)
	{
		if (g_StartClickStar->getStarPos().starRow - g_EndClickStar->getStarPos().starRow == 1 ||
			g_EndClickStar->getStarPos().starRow - g_StartClickStar->getStarPos().starRow == 1)
		{
			return true;
		}
	}
	return false;
}
//------------------------------------------------------------------------------------------------
void GameScene::swapStar()
{
	if (g_StartClickStar == g_EndClickStar)
	{
		return;
	}
	g_isAction = true;
	//记录位置
	Vec2 startPos = g_StartClickStar->getPosition();
	Vec2 endPos = g_EndClickStar->getPosition();
	//记录行号列号
	tagStarPos startTagPos = g_StartClickStar->getStarPos();
	tagStarPos endTagPos = g_EndClickStar->getStarPos();
	//交换行号列号
	g_StartClickStar->setStarPos(endTagPos);
	g_EndClickStar->setStarPos(startTagPos);
	//数据交换
	g_TotalStar[startTagPos.starRow][startTagPos.starCol] = g_EndClickStar;
	g_TotalStar[endTagPos.starRow][endTagPos.starCol] = g_StartClickStar;
	//数据交换完后，判断交换后能否消除
	//判断当前点击的星星上下左右有没有三个或以上的星星
	Vector<Star*>      StartVector_LR;
	CheckCoL(g_StartClickStar, StartVector_LR);
	Vector<Star*>      StartVector_UD;
	CheckRow(g_StartClickStar, StartVector_UD);
	//判断记录的星星上下左右有没有三个或三个以上的
	Vector<Star*>      EndVector_LR;
	CheckCoL(g_EndClickStar, EndVector_LR);
	Vector<Star*>      EndVector_UD;
	CheckRow(g_EndClickStar, EndVector_UD);
	//回调函数
	auto callFunc = CallFunc::create([&](){
		g_isAction = false;
	});
	if (StartVector_LR.size()>=3||
		StartVector_UD.size()>=3||
		EndVector_LR.size()>=3||
		EndVector_UD.size()>=3)
	{
		g_StartClickStar->runAction(Sequence::create
			(MoveTo::create(0.1, endPos),callFunc,NULL));
		g_EndClickStar->runAction(Sequence::create
			(MoveTo::create(0.1, startPos),callFunc, NULL));
		return;
	}
	//交换回行号列号
	g_StartClickStar->setStarPos(startTagPos);
	g_EndClickStar->setStarPos(endTagPos);
	//交换回数据
	g_TotalStar[startTagPos.starRow][startTagPos.starCol] = g_StartClickStar;
	g_TotalStar[endTagPos.starRow][endTagPos.starCol] = g_EndClickStar ;
	//如果不能消除，那么只要跑一下来回动作
	g_StartClickStar->runAction(Sequence::create(
		MoveTo::create(0.1, endPos),
		MoveTo::create(0.1, startPos), callFunc, NULL));
	g_EndClickStar->runAction(Sequence::create(
		MoveTo::create(0.1, startPos),
		MoveTo::create(0.1, endPos), callFunc, NULL));

}
//------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------