#ifndef _MYUTILS_H_
#define _MYUTILS_H_
#include "cocos2d.h"
USING_NS_CC;

#define		SCREEN_HEIGHT		Director::getInstance()->getVisibleSize().height			//屏幕高度
#define		SCREEN_WIDTH		Director::getInstance()->getVisibleSize().width				//屏幕高度

#define		MAX_ROW_NUM			8				//一行精灵个数
#define		MAX_COL_NUM			8				//一列精灵个数
#define		BOADER_WIDTH		2				//精灵间距

#define		SPRITE_WIDTH		72				//精灵的宽和高
#define		SPRITE_TOTAL		5				//精灵种类总数

#define		BIANJIE				SPRITE_WIDTH/2+(SCREEN_WIDTH - SPRITE_WIDTH * MAX_COL_NUM-BOADER_WIDTH*(MAX_COL_NUM-1))/2 //左边界
#define		XIABIANJIE			200				//下边界


const static char* SpritType[SPRITE_TOTAL] = 
{
	"Sprite0.png",
	"Sprite1.png",
	"Sprite2.png",
	"Sprite3.png",
	"Sprite4.png"
};
static int RandSeet()
{
	//获取系统时间
	timeval now;   //timeval 是个结构体，里面有两个变量，一个是以秒为单位，一个是以微妙为单位
	gettimeofday(&now, NULL);
	//初始化随机种子  
	unsigned long reed = now.tv_sec * 1000 + now.tv_usec / 1000;
	//srand()中传入一个随机数种子//
	srand(reed);

	return rand();
}
#endif