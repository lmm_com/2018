#include "StarSprite.h"
//-------------------------------------------------------------------------------------------
Star::Star()
	:s_isEnableRemove(false)
	, s_StarSprite(NULL)
	, s_RandSeet(0)
{
}
//-------------------------------------------------------------------------------------------
Star::~Star()
{

}
//-------------------------------------------------------------------------------------------
Star* Star::createStar(tagStarPos starPos)
{
	//创建星星对象
	auto star = Star::create();
	star->s_tagStarPos = starPos;
	star->s_StarColor = rand()%5;   //随机星星的颜色
	star->initStar();

	return star;
}
//-------------------------------------------------------------------------------------------
bool Star::init()
{
	if ( !Node::init() )
	{
		return false;
	}
	//随机种子
// 	if (unsigned(time(0)) - s_RandSeet)
// 	{
// 		srand(unsigned(time(0)));
// 		s_RandSeet = unsigned(time(0));
// 	}


	return  true;
}
//-------------------------------------------------------------------------------------------
void Star::initStar()
{
	CCLOG("s_StarColor = %d", s_StarColor);
	//根据星星的颜色创建星星精灵
	s_StarSprite = Sprite::create(SpritType[s_StarColor]);
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
	//s_StarSprite->setAnchorPoint(Vec2::ZERO);
#endif
	this->addChild(s_StarSprite);
}
//-------------------------------------------------------------------------------------------


//-------------------------------------------------------------------------------------------


//-------------------------------------------------------------------------------------------

//-------------------------------------------------------------------------------------------