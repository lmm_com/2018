#ifndef _GAMESCENE_H_
#define _GAMESCENE_H_
#include "cocos2d.h"
#include "StarSprite.h"
#include "MyUtils.h"

USING_NS_CC;

class GameScene :public Layer
{
public:
	GameScene();
	~GameScene();
	static Scene* createScene();
	virtual bool init();

	CREATE_FUNC(GameScene);
	//初始化星星
	void initStar();
	//判断有没有可消除的星星
	bool isThreeBorder();		//没有三个或三个以上连在一起的星星
	bool isDead();				//没有移动一步后可消除的星星

	//检查行列三个连在一起的星星
	void CheckRow(Star* s, Vector<Star*> &vec);
	void CheckCoL(Star* s, Vector<Star*> &vec);
	//判断两Vector的长度
	void CheckVectorSize(Vector<Star*>  &lrvec, Vector<Star*> &udvec);
	//创建星星
	void createGameStar(tagStarPos Vpos);
	//星星位置互转
	Vec2 StarPosition(tagStarPos Vpos);
	tagStarPos tagStarPosition(Vec2 vec);

	void meunBackCallback(Ref*  pSender);
	//update方法
	void update(float delta);
	//查看有能否被消除的星星
	void CheckAndReMoveStar();
	//消除星星
	void RemoveStar();
	//标记能被消除的星星
	void MarkReMoveStar(Star* star);
	//移除动作
	void RemoveStarAction(Node* node);
	//填充星星
	void FillStar();

	//触屏
	bool onTouchBegan(Touch *touch, Event *unused_event);
	void onTouchMoved(Touch *touch, Event *unused_event);
	void onTouchEnded(Touch *touch, Event *unused_event);
	//根据触摸的坐标得到精灵
	Star* PosWithStar(Vec2 vec);
	//判断两个精灵是否相邻
	bool isNearByStar();
	//交换两个精灵
	void SwapStar();
private:
	Star*			g_TotalStar[MAX_ROW_NUM][MAX_COL_NUM];

	bool			g_isAction;					//判断是否正在下落
	bool			g_isFillStar;				//判断是否需要填充精灵
	int  w;

	Vec2			g_StartTouchPos;				//开始触摸坐标
	Star*			g_StartClickStar;				//开始触摸的精灵
	Star*			g_EndClickStar;					//最后触摸的精灵
	bool			g_isEnableTouch;				//判断能否触摸
	bool			g_isMoveTouch;					//判断是否有移动触摸	
	bool			g_isTouchMoving;				//控制move只运行一次
};
#endif