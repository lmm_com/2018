#ifndef _GAMESCENE_H_
#define _GAMESCENE_H_
#include "cocos2d.h"
#include "StarSprite.h"
#include "MyUtils.h"

USING_NS_CC;

class GameScene :public Layer
{
public:
	GameScene();
	~GameScene();
	static Scene* createScene();
	virtual bool init();

	CREATE_FUNC(GameScene);
	//初始化星星
	void initStar();
	//判断有没有可消除的星星
	bool isThreeBorder();		//没有三个或三个以上连在一起的星星
	bool isDead();				//没有移动一步后可消除的星星

	//检查行列三个连在一起的星星
	void CheckRow(Star* s, Vector<Star*> &vec);
	void CheckCoL(Star* s, Vector<Star*> &vec);
	//创建星星
	void createGameStar(tagStarPos Vpos);
	//星星位置互转
	Vec2 StarPosition(tagStarPos Vpos);
	tagStarPos tagStarPosition(Vec2 vec);

	void meunBackCallback(Ref*  pSender);
	//update方法
	void update(float delta);
	//查看有能否被消除的星星
	void CheckAndReMoveStar();
	//消除星星
	void RemoveStar();
	//标记能被消除的星星
	void MarkReMoveStar(Star* star);
	//移除动作
	void RemoveStarAction(Node* node);
	//填充星星
	void FillStar();
	//触摸回调函数
	bool onTouchBegan(Touch *touch, Event *unused_event);
	void onTouchMoved(Touch *touch, Event *unused_event);
	void onTouchEnded(Touch *touch, Event *unused_event);
	//根据触摸位置得到当前触摸的星星
	Star* touchStar(Vec2 vPos);
	//判断两个星星是否相邻
	bool isNearStar();
	//交换星星
	void swapStar();
private:
	Star*			g_TotalStar[MAX_ROW_NUM][MAX_COL_NUM];
	bool			g_isAction;					//判断是否正在下落
	bool			g_isFillStar;				//判断是否需要填充精灵
	int  w;

	Star*			g_StartClickStar;			//开始触摸的精灵
	Star*			g_EndClickStar;				//记录触摸的精灵
	Vec2			g_StartTouch;				//开始触摸的坐标
	bool			g_isEnableTouch;			//判断能否触摸
	bool			g_isMoveTouch;				//表示滑动触摸
};
#endif