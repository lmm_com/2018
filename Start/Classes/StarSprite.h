#ifndef _STARSPRITE_H_
#define _STARSPRITE_H_
#include "cocos2d.h"
#include "MyUtils.h"
USING_NS_CC;

//星星的行号和列号的结构体
struct tagStarPos 
{
	int starRow;		//行号
	int starCol;		//列号
};
//星星的状态
enum StarStatu
{
	STATU_NORMAL,			//正常状态
	

	STATU_MAX
};

class Star :public Node
{
public:
	Star();
	~Star();
	//创建星星
	static Star* createStar(tagStarPos starPos);
	//初始化星星
	void initStar();
	virtual bool init();
	CREATE_FUNC(Star);

private:
	//星星位置
	CC_SYNTHESIZE(tagStarPos, s_tagStarPos, StarPos);
	//星星的颜色（类型）
	CC_SYNTHESIZE(int, s_StarColor, StarColor);
	//标记是否能被消除
	CC_SYNTHESIZE(bool, s_isEnableRemove, EnableRemove);
	Sprite*					s_StarSprite;
	int						s_RandSeet;		//随机种子
};

#endif