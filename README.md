﻿# 二维游戏引擎
## 开发环境
* Git
* python 2.x
* coco2d-x 引擎（3.x）
* vs2015
* JDK
* Android平台下开发
  * Apache Ant
  * Android SDK
  * Android NDK
* cocos2d-x引擎配置（python.setyp）
  * 引擎的配置（python setup.py）
  * 生成预设的环境
  * 生成支持预编译模板
* 创建cocos项目
  * `cocos new <项目名> -p <项目包名> -l cpp -t binary`
* Cocos2d-x项目结构
  * Classes --程序代码（C++）
     * AppDelegate类 --Cocos的入口类
  * Resousres --游戏资源（图片，音频，字体）
  * proj.xxx --对应平台的项目文件
## Cocos2d-x引擎
* 导演（Director）
  * 访问和变更场景
  * 访问Cocos2d-x的配置信息
  * 暂停，继续，停止游戏控制
  * 坐标系转换
* 场景（scene）
  * 场景分类
    * 展示类
	* 选项类场景
	* 游戏场景
  * 场景与层的关系
    * 一个场景（Scene）包含多个层（Layer）
  * 场景的控制
    * 运行：RUNWithScene
	* 切换场景（释放当前的场景）：replaceScene
	* 切换场景（当前场景入栈）：pushScene
	* 返回场景 ：popScene
    * 返回根场景:PopToRootScene
    * 场景过度动画 TransitionXXX
   * 场景的生命控制
    * onEnter
    * onExit
* 层（Layer）
 * 初始化调用：init方法 --层对象产生时调用一次
 * 进入层：onEnter
 * 退出层: onExit
 * 清除对象 ：cleanup方法
* 节点(Node)
 * 主要操作
  * 创建节点:create
  * 新增节点：addChild
  * 删除节点：removeChild/removeChildByTag/removeAllChildWithCleanup/removeFromParentAndCleanup
  * 查找节点:getChildByTag/getChildByName/getChildren
* 重要属性
  * 位置 position
  * 锚点 anchorPoint 默认是（0.5，0.5）即正中
* 定时器(Scheduler)
    * void scheduleUpdate(vpod) `会定时调用Update函数`
    * void schedule(SEL-SCHEDULE selector,float)
    * void unscheduleXXX 停止定时器
* 动作相关
 * 执行动作 runAction
 * 停止动作 stopAction/stopActionByTag/stopAllAction
* 坐标系
 * UI坐标系(Android/ios/Win的二维坐标系) `左上角`
 * OpenGL坐标系(世界坐标系/模型坐标系) `左上角`
* 文字
 * 字符串 std::string/cocos2d::_string `中文乱码情况严重`
 * 标签 Label/LabelAltas
  * 系统字体 createWithSystemFont
  * 指定字体 TTF字体  createWithTTF
  * 位图字体 图像 createWithBMFont
* 数据结构
 * 列表
 *  ———Array 数组集合
 *  Vector<T> 集合
 *  ValueVector
* 字典
 * ——Dictionary 字典
 * Map<K,V>
 * ValueMap/ValueMapIntKey
* 精灵（sprite）
  * 创建对象相关方法
   * create --创建后设置纹理属性
   * create(filename) --从指定资源（图片）创建资源
   * create(filename,rect) --从指定资源和指定区域创建精灵
   * create(texture) --从指定纹理创建精灵
   * create(texture,rect) --从创建纹理和区域创建精灵
   * createWithSpriteFram(spriteFrame) --从精灵帧创建精灵
   * createWithSpriteFrameName(spriteFram) --从精灵帧缓存指定
  * 性能优化
   * 纹理缓存（TextureCache）
   * 精灵帧缓存
   * 动画缓存（Anitimation）
* 菜单（Menu）
 * 菜单项（menuitem）
  * 文本菜单类 `MenuItemLabel`
  * 采用精灵菜单类 `menuItemSprite`
  * 开关菜单类 `MenuItemToggle`
* 动作（Action）
 * 基本动作
  * 瞬时动作 Place /Show/Hide/...
  * 间隔动作(By偏移和To目标) JUMP/Move/Rotate/Scale/Bezier
  * 间隔动作
 * 动作组合
  * 顺序序列（Sequence）
  * 并行序列（Spawn）
  * 重复序列（Repect）
  * 无需重复序列（RepeatForever）
  * 反序方法 reverse
 * 速度变化
 * 函数调用
  * 无参数调用:callFunc
  * 带参数调用：callFuncN
 * 动作跟随
 * 特效 网格动作
 * 动画
  * 动画类 Animation
  * 动作类 Animate
* 用户事件
 * 接收并处理的事件
  * 触摸事件
  * 加速度事件
  * 键盘事件
  * 鼠标事件
  * 自定义事件
 * 事件处理机制
  * 事件 Event
  * 事件源 Node
  * 事件处理者 EVent
   * 单点触摸事件处理 EventListenerTouchOneByOne
   * 多点触摸事件处理 
   * 键盘事件处理
   * 鼠标事件处理
   * 加速度事件处理
   * 游戏手柄事件处理
   * 焦点事件处理
   * 自定义事件处理
  * 事件分发器 EventDispatcher
   * 注册事件监听器
    * addEventListenWithSceneGraphPriority
    * addEventListenWithFixedPriority
   * 注销事件监听器
    * removeAllEventListeners
  * 在层中直接事件处理
   * setTouchXXX
   * setAccelermeterEnabled
 * 音频
  * Cocos2d-x跨平台音频的支持情况（音乐与音效）
  * Audio引擎 --SimpleAudioEngine
   * 处理背景音乐 xxxBackgroundMusic
   * 处理音效
 * 粒子系统
  * 粒子发射模式：重力模式，半径模式
  * 粒子系统的属性 ParticleXXX::create()
  * 自定义粒子系统
## 基于CoCos2d-x的二维游戏开发基础
* 创建项目
* 游戏主要元素
 * Hero 发射子弹
 * smallEnemy
 * BigEnemy 发射子弹
 * 背景
* 游戏操控：触摸控制
 * 触摸控制
 * 加速度计控制
 * 模拟摇杆
 * 手势控制
* AI控制
* 分辨率的控制：多分辨率的支持（程序支持以及资源的支持）
 * 横屏
 * 竖屏
* 发射子弹：持续的动作。采用调度器完成
* 游戏背景（不变的游戏背景，2个相同对象完成移动）
* 背景音乐与音效
* 碰撞检测
 * 矩形碰撞
 * AABB碰撞
 * 物理碰撞(Box2D引擎)
  * 相互碰撞的规则